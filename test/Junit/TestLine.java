package Junit;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestLine {

	@Test
	void tesSlopet() {
		Line line = new Line(4, 5, 6, 9);
		assertEquals(2,line.getSlope());
	}
	@Test
	void testDistance() {
		Line line = new Line(4, 5, 7, 8);
		assertEquals(4.242640687119285,line.getDistance());
	}
	@Test
	void parallelTo() {
		Line line = new Line(4, 5, 6, 9);
		Line l = new Line(4, 5, 6, 9);
		assertEquals( l.getSlope(), line.getSlope(), .0001 );
        
        
        
	}
}
