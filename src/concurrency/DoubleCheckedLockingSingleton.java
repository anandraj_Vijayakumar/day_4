package concurrency;

public class DoubleCheckedLockingSingleton {
	
	 
    
    private static DoubleCheckedLockingSingleton instance;   
 
    private DoubleCheckedLockingSingleton() 
    {
       
    }
    
    public static DoubleCheckedLockingSingleton getInstance() 
    {
        if (instance == null) 
        {
            synchronized (DoubleCheckedLockingSingleton.class) 
            {
                if (instance == null) 
                {
                    instance = new DoubleCheckedLockingSingleton();
                    System.out.println("Instance Created");
                }
            }
 
        }
        return instance;
    }
    	public static void main(String args[]) {
	
    		DoubleCheckedLockingSingleton.getInstance();
    	}
}

	